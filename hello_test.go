package main

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPostName(t *testing.T) {
	// Grab our router
	router := setupServer()

	data := url.Values{}
	data.Set("name", "Eunice")

	req, err := http.NewRequest("POST", "/names", strings.NewReader(data.Encode()))
	// resp, err := http.DefaultClient.Do(req)

	if err != nil {
		t.Fatal(err)
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	// use httptest.NewRecorder() as a mock of gin.Context.ResponseWriter
	rr := httptest.NewRecorder()

	// call your handler/server (http.ResponseWriter, *http.Request)
	// to deal wtih request and then respond
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, "Hello, Eunice. Welcome to my tiny service!", rr.Body.String())

	// c, _ := gin.CreateTestContext(rr)
	// controllers.PostName(c)
	// assert.Equal(t, http.StatusOK, rr.Code)

	// // Check the status code is what we expect.
	// if status := rr.Code; status != http.StatusOK {
	// 	t.Errorf("handler returned wrong status code: got %v want %v",
	// 		status, http.StatusOK)
	// }

	// // Check the response body is what we expect.
	// expected := "Hello, Eunice. Welcome to my tiny service!"
	// if rr.Body.String() != expected {
	// 	t.Errorf("handler returned unexpected body: got %v want %v",
	// 		rr.Body.String(), expected)
	// }
}
