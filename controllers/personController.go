package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetName ...
// Request: GET /names/{name}
// Response: Hello, {name}. This is GET.
func GetName(c *gin.Context) {
	name := c.Param("name")
	c.String(http.StatusOK, "Hello, %s. This is GET.", name)
}

// PostName ...
// Request: POST /names/{name}
// Response: Hello, {name}. Welcome to my tiny service!
func PostName(c *gin.Context) {
	name := c.PostForm("name")
	c.String(http.StatusOK, "Hello, %s. Welcome to my tiny service!", name)
}
